
var _ = require("underscore");
var fs = require("fs");
var ch =  require('./chess.js/chess.js');

var testMatchesFiles = fs.readdirSync(__dirname+ "/testData");

_.each(testMatchesFiles, function(fileName){

  var file = fs.readFileSync(__dirname + "/testData/" + fileName);
  var testData = JSON.parse(file);

  analyseThis(fileName, testData);

  fs.writeFileSync(__dirname + '/testResult/' + fileName, JSON.stringify(testData,null, 2));

});


function analyseThis(file, gameData){

  var engine = stockDolpinAdapter();
  //var engine = stockFishAdapter();  //todo


  console.log("========================================");
  console.log("Analysing " + file);
  console.log("========================================");



  var chess = ch.Chess();

  _.each(gameData, function(move){

    chess.move(move.move);

    move.result = {   //just appending the result on to the move..
        status : engine.getStatus(chess.fen(), 3000),
        suggestions : engine.getSuggestions(chess.fen(), 3000)
      };

    console.log("Analysed " + file + ". move " + move.move + ".  The result: \t" +  JSON.stringify(move.result));

  });
}

function stockDolpinAdapter(){

  return {
    getStatus:function(fen, timeToCalulate){

      return {white:Math.floor((Math.random() * 30) + 1), black:Math.floor((Math.random() * 20) + 1)}
    },
    getSuggestions : function(fen, timeToCalulate){

      var chess = new ch.Chess(fen);

      var pm = chess.moves(); //possible moves

      var suggestions = [
         {move:pm[0], chance:Math.floor((Math.random() * 20) + 1)},
         {move:pm[1], chance:Math.floor((Math.random() * 20) + 1)},
         {move:pm[2], chance:Math.floor((Math.random() * 20) + 1)}
      ];

      return suggestions;
    }
  }
}
