
//
// an easy made logger thats logg all request to memory
//

var moment = require("moment");
var _ = require("underscore");

var requestLog = [];

exports.log = function(req, res, next) {
    if(req.originalUrl != "/log"){
      requestLog.push(moment().format("LLLL") + " Path: " + req.originalUrl);
      if (requestLog.length>20) requestLog.shift();
    }
    next(); // Passing the request to the next handler in the stack.
}

exports.getAll = function(){
  return requestLog;
}
