
var port = process.env.PORT || 3009;

// basic core
var express = require('express');
var bodyParser = require("body-parser");
var logger = require("./instrumentation/trondsLogger");
var app = express();

// helper/utils
var moment = require("moment");
var Mustache = require("mustache");
var ch = require("./chessjs/chess.js");
var _ = require("underscore");

// sources for gamedata
var randomSource = require("./pgnSource/randomGames");
var recordingSource = require("./pgnSource/recordingSource");
var pgnUrlSource = require("./pgnSource/pgnUrlSource");

var tournamentDb = require("./tournamentDb/tournamentDb");

var ongoingRounds = [];

app.use(express.static(__dirname + '/public'));

app.use(logger.log);
app.use(bodyParser());


console.log("the dolphins are listen on port: " + port);
app.listen(port);

var databaseRounds = tournamentDb.getAll();



app.get("/", status);

app.get("/log", function(req, res){
  var resp = "";
  _.each(logger.getAll(), function(rli){
    resp+= "<li>" + JSON.stringify(rli) + "</li>";
  });
  res.send("<ul>" + resp + "</ul>");
});


app.get("/stop/:id", function(req, res){
  var id = req.params.id;
  var theRound = _.findWhere(ongoingRounds, {id:id});

  switch(theRound.type){
    case "Random":
      randomSource.stop(theRound);
      break;
  }

  ongoingRounds = _.without(ongoingRounds, _.findWhere(ongoingRounds, {id:id}));

  res.redirect("/status");
});


app.get("/jump/:tournamentId/:roundId/:minutes", function(req, res){

  var t = req.params.tournamentId;
  var r = req.params.roundId;

  var theRound = _.findWhere(databaseRounds, {tournamentId: t, roundId : r});

  theRound.started = theRound.started.subtract("minutes", req.params.minutes);

  res.redirect("/status");
});

app.get("/status", status);

app.get("/feed/:roundid/:gameid", function(req, res){
  var roundId = req.params.roundid;
  var gameId = req.params.gameid;
  var pgnText = getPgnFeed(roundId, gameId);
  res.send(pgnText);
});

app.get("/feed/:roundid", function(req, res){
  console.log("route - round");
  var roundId = req.params.roundid;
  var pgnText = getPgnFeed(roundId, null);
  res.send(pgnText);
});


app.get("/feed/:id", function(req, res){
  var id = req.params.id;
  var pgnText = getPgnFeed(id, null);
  res.send(pgnText);
});


function getPgnFeed(roundId, gameId){

  var theRound = _.findWhere(ongoingRounds, {id:roundId});
  var pgnText = "";
  switch(theRound.type){
    case "Random":
      pgnText = randomSource.getFeed(theRound, gameId);
      break;
    case "fromUrl":
      pgnText = pgnUrlSource.getFeed(theRound, gameId);
      break;
    default:
      res.send("not supported!!!" );
      return;
  }
  return pgnText;
}

app.get("/dbgame/:tournamentId/:roundId", function(req, res){

  var t = req.params.tournamentId;
  var r = req.params.roundId;

  var theRound = _.findWhere(databaseRounds, {tournamentId:t, roundId : r});

  console.log("feed-: " + theRound.tournamentId + "/" +  theRound.roundId);

  if(!theRound.started){
    res.send("Round " +  theRound.tournamentId + "/" + theRound.roundId + " not started\n");
    return;
  }

  var rightNow = moment();
  var secondsLapsed = rightNow.diff(theRound.started, "seconds");

  if (theRound.tournamentId == "worldchamp2013"){
    if (theRound.roundId == "normal" ){
      var resText = playWc(secondsLapsed, theRound, 60, req, res);
      res.send(resText);
    }
    else if(theRound.roundId === "blitzSpeed"){
      var resText = playWc(secondsLapsed, theRound, 10, req, res);
      res.send(resText);
    }
  }
  else{
    findfrompgnfiles(secondsLapsed, theRound, req, res);
  }

});

app.post("/", function(req, res){

  var si = req.body;

  console.log(si);

  switch(si.gameSourceRadio){
    case 'fromDb':
      var thes = si.gameDb.split("|");
      ongoingRound.push()
      startDb(thes[0],thes[1]);
      res.redirect("/status");
      break;
    case 'fromRandom':
      ongoingRounds.push(randomSource.startGames(si.numberOfGames, si.secondsPrMove));
      res.redirect("/status");
      break;
    case "fromUrl":
      pgnUrlSource.startGames(si.pgnUrl, si.secondsPrMove, function(round){
        ongoingRounds.push(round);
        res.redirect("/status");
      });
      break;
    default:
      throw "not implemented";

}


});


// --------------
// functions
// --------------


function playWc(secondsLapsed, theRound, secondsPrMove, req, res){

  secondsLapsed = secondsLapsed - 120;

  var fs = require('fs');

  var gamesInWc = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10"];
  var totalResult = "";
  _.each(gamesInWc, function(gameId){

      var path = __dirname + '/public/wcJson/' + gameId + ".json";

      console.log("path: " + path);

      var data = fs.readFileSync(path, 'utf8');

      var theGame = JSON.parse(data);

      var resText = '[Event "World champ 2013"]\n';
      resText += '[White "Player w' + gameId + '"]\n';
      resText += '[Black "Player b' + gameId + '"]\n';

      var moveNo = 0;
      var whiteInMove = true;

      var numberOfMovesPlayed = Math.round(secondsLapsed / secondsPrMove);

      var fg = _.first(theGame.halgScore, numberOfMovesPlayed);
      var sjakk = ch.Chess();

      var pgnA = "";
      var prePgn = "";

      _.each(fg, function(m){
          if (whiteInMove){
            whiteInMove = false;
            moveNo = moveNo + 1;
          }
          else{
            whiteInMove = true;
          }

          var ret = sjakk.move({from: m.substr(0,2), to : m.substr(3,2)});

          var pgn = sjakk.pgn();

          pgnA += pgn.substr(prePgn.length) + " {[%clk 0:28:51]}";

          prePgn = pgn;
      });

      totalResult += resText + pgnA + (theGame.halgScore.length<numberOfMovesPlayed ? " 1-0" : "*");
  });
  console.log("return: " + totalResult);
  return totalResult;
}

function status(req, res){

  var fs = require("fs");

  var vm = {
    tournamentRounds: tournamentDb.getAll(),
    rounds : ongoingRounds.map(function(i){return {id:i.id, name:i.name, started:moment(i.started).fromNow()}})
  };

  var template = fs.readFileSync(__dirname +"/views/status.html", {encoding:"utf-8"});

  res.send(Mustache.render(template,vm));
}

function findfrompgnfiles(secondsLapsed, theRound, req, res){

  //find file

  var find = secondsLapsed;

  var path = __dirname + '/public/gamedata/' + theRound.tournamentId + "/" + theRound.roundId;


  var fs = require('fs');
  while(find>0){
    if (fs.existsSync(path+ "/games-" + find + ".pgn")) {
      break;
    }
    else{
      find--;
    }
  }

  if (find<0){
    console.log("file not found!");
    return;
  }

  console.log("found file:" + find)


  fs.readFile(path + "/games-"+ find +".pgn", 'utf8', function (err,data) {
    if (err) {
      res.send(err);
    }
    res.send(data);
  });
}
