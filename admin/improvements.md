
"Try yourself"
==

Three variants:

1) Analytic board

"Take over" the current board, and try out some moves from there. To play with variants. May be add arrows, annotation and so on

2) Play against Stockfish from here

"Take over" the current board, and play this position against Stockfish.

3) Play against a friend

Challenge a friend to play from current board. (more to be specified)

```
Benefit: User engagement
```

Radio-mode on mobile
==

A lot of the user we have talked to have missed the opportunity to listen to the commentaries while looking at the board.

The chess games are looong and many wants to do other things, but still following the game.

```
Benefit: Enhance the experience to other "time boxes" (like housekeeping and commute)
```

Annotation of game highlights
==

Using visual "signals" to show the critical positions.

Two ways to do this(not mutally exclusives)

 1) Automatic: When the prosentages are changes beond some predefined limits
 2) Manuelt: Commentators can set a mark of highlights.

```
Benefit: Improve the understaning of the game. More compelling to "browse" the game.
```

Vote for end result
==

How will this game end?

Carlsen winning: [button]
Draw: [button]
Topolov winning [button]

```
Benefit: Engage the users. and... may be this will make a better estimate than stockfish
```

"Guess the next move"
==

List all legal moves, and let the users guess whats next move.

Make a contest: The best "guesser" wins an Ipad.

```
Benefit: Engage user, and make them stay "on the channel" even through commersial break
```

Improved Standard modus
==

- Click on a piece to see it's legal moves
- Show all threats for white/black (select)

```
Benefit: Engage user. Better game understanding.
```

Introduce Expert Modus
==

For the chess nerds:

- Show centipawn instead of %
- Show san-notation on move history
- Show Stockfish move sequence for suggested moves.

```
Benefit: Attracting the nerds
```

Improve estimation
==

The algoritm can be improved

- Fine tune % calculation
- Cahnge scheduling to goInfinit->Stop, goInfinit->Stop

```
Benefit: Improve our position of trust
```

Statistic information
==

Show statistci information:

- Showing rating over time
- Previous games these to players have had
- other?

```
Benefi: Visualise the contekst of the game
```

Prepare for vm
==

- Improved GUI to capture the "drama" in the match
  - Showing the development of the game
  - Status on match
  - previous and upcomming games
- Technical integration with arranger.

```
Benefit: Necessary to send this match
```

Prepare for Chess Olympics
==

How do we present this turnament?

- Only the game of the norwegians?
- Integration
- Easier administration:
  - No need for manual player registration
  - Choose which games get analysed and available
  - Configure analysis-options (how deep/how much time for shallow/deep)
- Manual leaderboard

```
Benefit: Necessary to send this tournament
```

Refactoring
==

Previous project was under short timelimits. We need to do some refactoring to enable good progress ahead.

- Improved autoscaling of processor utilization
- Clean up structure: Round, game...
- Sort the games in a round instead of just one featured game.

```
Benefit: Improved quality. Ensure stable codebase and a platform for future development.
```
