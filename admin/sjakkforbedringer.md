
"Overta brettet"
==

Tre varianter:

1) Analysebrett

Overta brettet i stillingen, og gjør trekk for analyse. Grafikk også? Piler etc

2) Spill videre mot Stockfish

Overta brettet som hvit eller svart, og spill videre mot Stockfish.

3) Spill videre mot annen person

utfordre en kompis... [mer her...]

```
Gevinst: Egansjere brukerne slik at de blir på "kanalen".
```

Radio-modus på mobil
==

Mange vi har pratet med har savnet å kunne ha lyd på mens de ser på livesenteret på mobil ("husarbeidsmodus").

```
Gevinst: Utvide bruken av løsningen til andre tidsluker (pendletid, husarbeid etc)
```

Markering av høydepunkter i parti
==

For å synliggjøre "dramatikken" i partiene kan vi sette opp "signaler" i pianobaren og/eller trekkhistorikken for å vise vendepunkter i partiene.

Det er to måter å gjør dette på:

 1) Automatisk: Hvis prosentene beveger seg utenfor visse marginer.
 2) Manuelt: Kommentator kan markere et trekk som vendepunkt.

```
Gevinst: Forbedrer opplevelsen i forhold til konkurrenten
```

Stemme på utfall
==

Hva tror du utfallet blir

Carlsen vinner: [knapp]
Remis: [knapp]
Topolov vinner [knapp]

```
Gevinst: Engasjerer brukerne. (og kan vise et bedre estimat enn stockfish)
```

"Gjett neste trekk"
==

Liste opp alle lovlige trekk (evnt utvalgte) og la brukerne gjette hvilket trekk som er neste.

Lage en konkurranse: Den som gjetter best vinner en Ipad.

```
Gevinst: Engasjerer brukerne.
```

Forbedret standardmodus
==

- Kunne klikke på en brikke og se hvor den kan flytte.
- Vise alle trussler til valgt farge

```
Gevinst: Forbedre brukeropplevelsen ytterligere
```

Ekspertmodus
==

For nerdene:

- Vis centipawns-score i stedet for %
- Vis san-notasjon i trekkhistorikk
- Vis Stockfish trekksekvens for foreslåtte trekk

```
Gevinst: Kapre nerdene
```

Forbedre utfalls-estimering
==

Dagens algoritmer kan forbedres.

- Tune beregning fra sentipawns til prosent.
- Endre scheduling til goInfinit->Stop, goInfinit->Stop

```
Gevinst: Foredre tiliten til estimatene
```

Spillerstatistikk
==

Forbedret visning av statistikk:

- Utvikling av rating over tid player vs player
- Tidligere oppgjør mellom spillerne
- andre?

```
Gevinst: Visualisere konteksten til oppgjøret.
```

Klargjøring til VM
==

- Endret GUI for å fange dramatikken i VM matchen
  - Formkurve i match
  - Status på match
  - historikk og kommende parti
- Teknisk integrasjon mot arrangøren (manuell "fallback"?)

```
Gevinst: Nødvendig for å kunne gi en god formidling av VM-kampene.
```

Klargjøring til Sjakk Olympiaden
==

Hvordan presenterer vi dette?

- Kun nordmennenes kamper?
- Integrasjon
- Enklere administrasjon:
  - Ikke nødvendig å manuelt registrere spillere
  - Velge hvilke partier som skal analyseres og vises i frontend
  - Konfigurere analyse (hvor dyp/hvor lenge for shallow/deep analyse) (Classic 3 / 30), Bliz/rapid(3 sek)
  - Konfigurere tidskontroll
  - Endre kø til LIFO
- Manuell stillingsoversikt, definere + implementere.

```
Gevinst: Nødvendig for å formidle Sjakk Olympiaden.
```

Refactoring
==

Forrige prosjekt ble gjort med veldig knapp tidsmargin. Vi ønsker å bruke noe tid på å rydde opp i koden og restrukturere slik at vi kan lette arbeidet videre...

- Forbedret funksjonalitet for opp- og nedskallering av prosessorkraft i forhold til behov
- Rydde i runde/spill struktur
- Sortering av parti i runde i stedet for featured game

```
Gevinst: Forbedret kvalitet. Muliggjør videreutvikling.
```
