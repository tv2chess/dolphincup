

var ch = require("./chessjs/chess.js");
var _ = require("underscore");

var dirs = ["wc01"];

var fs = require("fs");

_.each(dirs, function(el){

  console.log("----------\n" + el);

  var rootDir = __dirname + "/public/gamedata/" + el ;

  var files = fs.readdirSync(rootDir);

  var sum = 0;
  var errors = 0;

  _.each(files, function(f){

      var pgnFile = fs.readFileSync(rootDir + "/" + f, "utf-8");

      var pgns = pgnFile.split("[Event");

      _.each(pgns, function(pgn){

          var sjekkPgn = "[Event" + pgn;

          var sjakk = new ch.Chess();
          var result = sjakk.load_pgn(pgnFile);

          sum += 1;

         if(!result){
           console.log( f + " " + result);
           errors++;
         }
         else{
           console.log( f + " " + result);
         }

      });

  });

  console.log("Summary: ");
  console.log("errors: " + errors);
  console.log("sum:" + sum);

});
