***
Chess Livesenter
***

**Dolphin Cup**

For recording, replay and analyses of pgn's

http://dolphincup.azurewebsites.net

***

*Development*

[Admin Portal](http://54.195.245.106/)

[Frontend](http://2beta.no/sjakktest/)

*Production*

[Admin Portal](http://tv2-chess-backend-1806352628.eu-west-1.elb.amazonaws.com/)

[Frontend](http://www.tv2.no/sport/sjakk/livesenter/)
