
var cuid = require('cuid');
var _ = require("underscore");
var ch = require("../chessjs/chess.js");
var request = require("request");

exports.startGames = function (url, secondsPrMove, callback){
  console.log("start - url");
  var id = cuid();
  var theRound = {
    id: id,
    type : "fromUrl",
    name: "From "  + url,
    started : new Date(),
    fromPgnRound : {
        url: url,
        secondsPrMove : secondsPrMove,
        games : [],
        numberOfMovesDone : 0
    }
  }

  request(url, function(error, response, body) {

    var gamesArray = body.split('[Event ');

    for(var i=0;i<gamesArray.length;i++){

      var chess = new ch.Chess();
      var fullPgn = '[Event ' + gamesArray[i];

      var valid = chess.load_pgn(fullPgn);
      var headerInfo = _.pairs(chess.header());
      var hist = chess.history();
      chess.load("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
      if (!valid)
      {
        console.log("invalid pgn: " + fullPgn);
      }
      else
        {
        theRound.fromPgnRound.games[i] = {
          finished : false,
          result : "",
          header : headerInfo,
          history : hist,
          theGame : chess
        }
      }
      headerInfo.forEach(function(item){
        if(item[0]=="Result"){
          theRound.fromPgnRound.games[i].result = item[1];
          console.log("Fant result: " + item[1]);
        }
        else{
          chess.header(item[0], item[1]);
        }
      });
      console.log("pgn n�����" + chess.pgn());

    }

    callback(theRound);
  });
}

exports.stop = function(theRound){
  console.log("stopped!");
}

exports.getFeed = function (theRound, gameId){


  var pgnText = "";

  var now = new Date();
  var secondsPlayed = Math.floor((now.getTime() - theRound.started.getTime()) / 1000);

  var expectedMovesToNow = Math.floor(secondsPlayed / theRound.fromPgnRound.secondsPrMove);

  var numberOfMovesToBeDone = expectedMovesToNow - theRound.fromPgnRound.numberOfMovesDone;


  console.log("-----------------------------------------------------");
  console.log("expected moves to now: " + expectedMovesToNow);
  console.log("-----------------------------------------------------");

  console.log("number of games:  " + theRound.fromPgnRound.games.length);

  var theGameId=0;
  _.each(theRound.fromPgnRound.games, function(game){

    theGameId++;

    if (gameId && gameId != theGameId)
      return; //next

    var thisGameExpectedMoves = expectedMovesToNow;
    if (expectedMovesToNow>game.history.length){

      thisGameExpectedMoves;
      game.finished=true;
    }
    console.log("Is the end here? " + expectedMovesToNow + ", " + game.history.length);

    while(game.theGame.history().length<thisGameExpectedMoves){
      if (!game.history[game.theGame.history().length])
        break;
      console.log("playing: " + game.history[game.theGame.history().length]);
      game.theGame.move(game.history[game.theGame.history().length]);
    }
    var stat = (game.finished?game.result : "*");
    pgnText +=  game.theGame.pgn() + " " + stat + "\n\n";

  });

  return pgnText;
}
