
var cuid = require('cuid');
var _ = require("underscore");
var ch = require("../chessjs/chess.js");
var sillyName = require("sillyname");

exports.startGames = function (numberOfGames, secondsPrMove){

  var id = cuid();
  var theRound = {
    id: id,
    type : "Random",
    name: "Random (" + numberOfGames + " games, with " + secondsPrMove + " sec pr move)",
    started : new Date(),
    randomRound : {
        numberOfGames: numberOfGames,
        secondsPrMove : secondsPrMove,
        games : [],
        numberOfMovesDone : 0
    }
  }

  for(var i=0;i<numberOfGames;i++){

    theRound.randomRound.games[i] = {
      white: sillyName(),
      black: sillyName(),
      finished : false,
      result : "",
      theGame : ch.Chess()
    }

  }

  theRound.theTicker = setInterval(function() {doMoves(theRound)}, 1000);

  return theRound;
}

exports.stop = function(theRound){
  clearInterval(theRound.theTicker);
  console.log("stopped!");
}

exports.getFeed = function (theRound, gameId){

  var pgnText = "";

  var theGameId=0;
  _.each(theRound.randomRound.games, function(game){

    theGameId++;

    if (gameId && gameId != theGameId)
      return; //next

    pgnText += '[Event "Random game ' + theRound.id + '"]\n';
    pgnText += '[White "'+ game.white +'"]\n';
    pgnText += '[Black "' + game.black + '"]\n';
    pgnText += '[BlackElo "2513"]\n';
    pgnText += '[WhiteElo "2390"]\n';

    pgnText += game.theGame.pgn();

    if (game.finished){
      if (game.result=="d")
        pgnText += " 1/2-1/2 ";
      else if (game.result=="w")
        pgnText += " 1-0";
      else
        pgnText += " 0-1";
    }
    else{
      pgnText+= " *";
    }

    pgnText += '\n\n';
  });

  return pgnText;
}

function doMoves(theRound){

  var now = new Date();
  var secondsPlayed = Math.floor((now.getTime() - theRound.started.getTime()) / 1000);

  var expectedMovesToNow = Math.floor(secondsPlayed / theRound.randomRound.secondsPrMove);

  var numberOfMovesToBeDone = expectedMovesToNow - theRound.randomRound.numberOfMovesDone;
  theRound.randomRound.numberOfMovesDone = expectedMovesToNow;

  console.log("-----------------------------------------------------");
  console.log("doMoves: (numberOfMovesDone: " + theRound.randomRound.numberOfMovesDone + ")");
  console.log("-----------------------------------------------------");

  var allFinished = true;

  _.each(theRound.randomRound.games, function(game){

    if (game.finished){
      console.log("finished. (" + game.result + ")");
      return;
    }

    allFinished = false;

    for(var t=0;t<numberOfMovesToBeDone;t++){

      var possibleMoves = game.theGame.moves();

      // exit if the game is over
      if (game.theGame.game_over() === true || game.theGame.in_draw() === true || possibleMoves.length === 0) {
        game.finished = true;

        if (game.theGame.in_draw())
          game.result="d";
        else{
          if (game.theGame.turn()==="b" && game.theGame.in_checkmate())
            game.result="w";
          else if (game.theGame.turn()==="w" && game.theGame.in_checkmate())
            game.result="b";
        }
        console.log("ended! (" + game.result + ")");
      }
      else{
        var randomIndex = Math.floor(Math.random() * possibleMoves.length);
        console.log("doing: " + possibleMoves[randomIndex]);
        game.theGame.move(possibleMoves[randomIndex]);
      }
    }
  });

  if (allFinished){
    clearInterval(theRound.theTicker);
    console.log("All Finished!");
  }
}
