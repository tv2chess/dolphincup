
exports.getAll = function(){

  var databaseRounds = [];

  databaseRounds.push({tournamentId: "copenhagen-2014", roundId:"05", started:null});
  databaseRounds.push({tournamentId: "copenhagen-2014", roundId:"06", started:null});
  databaseRounds.push({tournamentId: "copenhagen-2014", roundId:"07", started:null});
  databaseRounds.push({tournamentId: "copenhagen-2014", roundId:"08", started:null});
  databaseRounds.push({tournamentId: "copenhagen-2014", roundId:"09", started:null});
  databaseRounds.push({tournamentId: "worldchamp2013", roundId:"normal", started:null});
  databaseRounds.push({tournamentId: "worldchamp2013", roundId:"blitzSpeed", started:null});
  databaseRounds.push({tournamentId: "norwaychess-2014-classical", roundId:"01", started:null});
  databaseRounds.push({tournamentId: "norwaychess-2014-classical", roundId:"02", started:null});
  databaseRounds.push({tournamentId: "norwaychess-2014-classical", roundId:"03", started:null});
  return databaseRounds;

}
